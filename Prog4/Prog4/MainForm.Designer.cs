﻿namespace Prog4
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         this.components = new System.ComponentModel.Container();
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
         this.menuFileLoadFigures = new System.Windows.Forms.ToolStripMenuItem();
         this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
         this.glControl1 = new OpenTK.GLControl();
         this.trackBarEyeX = new System.Windows.Forms.TrackBar();
         this.trackBarEyeY = new System.Windows.Forms.TrackBar();
         this.trackBarEyeZ = new System.Windows.Forms.TrackBar();
         this.labelEyeX = new System.Windows.Forms.Label();
         this.labelEyeY = new System.Windows.Forms.Label();
         this.labelEyeZ = new System.Windows.Forms.Label();
         this.btnEyeReset = new System.Windows.Forms.Button();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.trackBarTimer = new System.Windows.Forms.TrackBar();
         this.labelTimer = new System.Windows.Forms.Label();
         this.movementTimer = new System.Windows.Forms.Timer(this.components);
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.btnLightReset = new System.Windows.Forms.Button();
         this.labelLightZ = new System.Windows.Forms.Label();
         this.labelLightY = new System.Windows.Forms.Label();
         this.labelLightX = new System.Windows.Forms.Label();
         this.trackBarLightZ = new System.Windows.Forms.TrackBar();
         this.trackBarLightY = new System.Windows.Forms.TrackBar();
         this.trackBarLightX = new System.Windows.Forms.TrackBar();
         this.comboBoxLightColor = new System.Windows.Forms.ComboBox();
         this.colorlabel = new System.Windows.Forms.Label();
         this.trackBarAmbient = new System.Windows.Forms.TrackBar();
         this.labelAmbient = new System.Windows.Forms.Label();
         this.groupBox3 = new System.Windows.Forms.GroupBox();
         this.groupBox4 = new System.Windows.Forms.GroupBox();
         this.menuStrip1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarEyeX)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarEyeY)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarEyeZ)).BeginInit();
         this.groupBox1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarTimer)).BeginInit();
         this.groupBox2.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarLightZ)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarLightY)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarLightX)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarAmbient)).BeginInit();
         this.groupBox3.SuspendLayout();
         this.groupBox4.SuspendLayout();
         this.SuspendLayout();
         // 
         // menuStrip1
         // 
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Size = new System.Drawing.Size(721, 24);
         this.menuStrip1.TabIndex = 0;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // menuFile
         // 
         this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileLoadFigures,
            this.menuFileExit});
         this.menuFile.Name = "menuFile";
         this.menuFile.Size = new System.Drawing.Size(37, 20);
         this.menuFile.Text = "&File";
         // 
         // menuFileLoadFigures
         // 
         this.menuFileLoadFigures.Name = "menuFileLoadFigures";
         this.menuFileLoadFigures.Size = new System.Drawing.Size(141, 22);
         this.menuFileLoadFigures.Text = "&Load Figures";
         this.menuFileLoadFigures.Click += new System.EventHandler(this.menuFileLoadFigures_Click);
         // 
         // menuFileExit
         // 
         this.menuFileExit.Name = "menuFileExit";
         this.menuFileExit.Size = new System.Drawing.Size(141, 22);
         this.menuFileExit.Text = "E&xit";
         this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
         // 
         // glControl1
         // 
         this.glControl1.BackColor = System.Drawing.Color.Black;
         this.glControl1.Location = new System.Drawing.Point(12, 267);
         this.glControl1.Name = "glControl1";
         this.glControl1.Size = new System.Drawing.Size(500, 500);
         this.glControl1.TabIndex = 1;
         this.glControl1.VSync = false;
         // 
         // trackBarEyeX
         // 
         this.trackBarEyeX.LargeChange = 20;
         this.trackBarEyeX.Location = new System.Drawing.Point(25, 48);
         this.trackBarEyeX.Maximum = 250;
         this.trackBarEyeX.Minimum = -250;
         this.trackBarEyeX.Name = "trackBarEyeX";
         this.trackBarEyeX.Size = new System.Drawing.Size(125, 45);
         this.trackBarEyeX.TabIndex = 0;
         this.trackBarEyeX.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarEyeX.Value = 50;
         this.trackBarEyeX.ValueChanged += new System.EventHandler(this.trackBarX_ValueChanged);
         // 
         // trackBarEyeY
         // 
         this.trackBarEyeY.LargeChange = 20;
         this.trackBarEyeY.Location = new System.Drawing.Point(152, 46);
         this.trackBarEyeY.Maximum = 250;
         this.trackBarEyeY.Minimum = -250;
         this.trackBarEyeY.Name = "trackBarEyeY";
         this.trackBarEyeY.Size = new System.Drawing.Size(125, 45);
         this.trackBarEyeY.TabIndex = 1;
         this.trackBarEyeY.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarEyeY.Value = 50;
         this.trackBarEyeY.ValueChanged += new System.EventHandler(this.trackBarY_ValueChanged);
         // 
         // trackBarEyeZ
         // 
         this.trackBarEyeZ.LargeChange = 20;
         this.trackBarEyeZ.Location = new System.Drawing.Point(283, 46);
         this.trackBarEyeZ.Maximum = 250;
         this.trackBarEyeZ.Minimum = -250;
         this.trackBarEyeZ.Name = "trackBarEyeZ";
         this.trackBarEyeZ.Size = new System.Drawing.Size(125, 45);
         this.trackBarEyeZ.TabIndex = 2;
         this.trackBarEyeZ.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarEyeZ.Value = 50;
         this.trackBarEyeZ.ValueChanged += new System.EventHandler(this.trackBarZ_ValueChanged);
         // 
         // labelEyeX
         // 
         this.labelEyeX.AutoSize = true;
         this.labelEyeX.Location = new System.Drawing.Point(22, 27);
         this.labelEyeX.Name = "labelEyeX";
         this.labelEyeX.Size = new System.Drawing.Size(41, 13);
         this.labelEyeX.TabIndex = 5;
         this.labelEyeX.Text = "X = 5.0";
         // 
         // labelEyeY
         // 
         this.labelEyeY.AutoSize = true;
         this.labelEyeY.Location = new System.Drawing.Point(149, 27);
         this.labelEyeY.Name = "labelEyeY";
         this.labelEyeY.Size = new System.Drawing.Size(41, 13);
         this.labelEyeY.TabIndex = 6;
         this.labelEyeY.Text = "Y = 5.0";
         // 
         // labelEyeZ
         // 
         this.labelEyeZ.AutoSize = true;
         this.labelEyeZ.Location = new System.Drawing.Point(280, 27);
         this.labelEyeZ.Name = "labelEyeZ";
         this.labelEyeZ.Size = new System.Drawing.Size(41, 13);
         this.labelEyeZ.TabIndex = 7;
         this.labelEyeZ.Text = "Z = 5.0";
         // 
         // btnEyeReset
         // 
         this.btnEyeReset.Location = new System.Drawing.Point(419, 68);
         this.btnEyeReset.Name = "btnEyeReset";
         this.btnEyeReset.Size = new System.Drawing.Size(75, 23);
         this.btnEyeReset.TabIndex = 3;
         this.btnEyeReset.Text = "Reset";
         this.btnEyeReset.UseVisualStyleBackColor = true;
         this.btnEyeReset.Click += new System.EventHandler(this.btnEyeReset_Click);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.btnEyeReset);
         this.groupBox1.Controls.Add(this.trackBarEyeZ);
         this.groupBox1.Controls.Add(this.labelEyeZ);
         this.groupBox1.Controls.Add(this.trackBarEyeX);
         this.groupBox1.Controls.Add(this.labelEyeY);
         this.groupBox1.Controls.Add(this.trackBarEyeY);
         this.groupBox1.Controls.Add(this.labelEyeX);
         this.groupBox1.Location = new System.Drawing.Point(12, 43);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(500, 100);
         this.groupBox1.TabIndex = 9;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "X, Y, Z values of the View Reference Point (Eyeball) looking at (0,0,0) with Y up" +
    "";
         // 
         // trackBarTimer
         // 
         this.trackBarTimer.Location = new System.Drawing.Point(21, 47);
         this.trackBarTimer.Maximum = 100;
         this.trackBarTimer.Name = "trackBarTimer";
         this.trackBarTimer.Size = new System.Drawing.Size(125, 45);
         this.trackBarTimer.TabIndex = 10;
         this.trackBarTimer.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarTimer.ValueChanged += new System.EventHandler(this.trackBarTimer_ValueChanged);
         // 
         // labelTimer
         // 
         this.labelTimer.AutoSize = true;
         this.labelTimer.Location = new System.Drawing.Point(18, 25);
         this.labelTimer.Name = "labelTimer";
         this.labelTimer.Size = new System.Drawing.Size(64, 13);
         this.labelTimer.TabIndex = 11;
         this.labelTimer.Text = "Speed = 0%";
         // 
         // movementTimer
         // 
         this.movementTimer.Tick += new System.EventHandler(this.movementTimer_Tick);
         // 
         // groupBox2
         // 
         this.groupBox2.AccessibleName = "LightPosition";
         this.groupBox2.Controls.Add(this.btnLightReset);
         this.groupBox2.Controls.Add(this.labelLightZ);
         this.groupBox2.Controls.Add(this.labelLightY);
         this.groupBox2.Controls.Add(this.labelLightX);
         this.groupBox2.Controls.Add(this.trackBarLightZ);
         this.groupBox2.Controls.Add(this.trackBarLightY);
         this.groupBox2.Controls.Add(this.trackBarLightX);
         this.groupBox2.Location = new System.Drawing.Point(12, 153);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(500, 100);
         this.groupBox2.TabIndex = 12;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "X, Y, Z values of the point light source";
         // 
         // btnLightReset
         // 
         this.btnLightReset.Location = new System.Drawing.Point(419, 71);
         this.btnLightReset.Name = "btnLightReset";
         this.btnLightReset.Size = new System.Drawing.Size(75, 23);
         this.btnLightReset.TabIndex = 7;
         this.btnLightReset.Text = "Reset";
         this.btnLightReset.UseVisualStyleBackColor = true;
         this.btnLightReset.Click += new System.EventHandler(this.btnLightReset_Click);
         // 
         // labelLightZ
         // 
         this.labelLightZ.AutoSize = true;
         this.labelLightZ.Location = new System.Drawing.Point(280, 22);
         this.labelLightZ.Name = "labelLightZ";
         this.labelLightZ.Size = new System.Drawing.Size(47, 13);
         this.labelLightZ.TabIndex = 5;
         this.labelLightZ.Text = "Z = 10.0";
         // 
         // labelLightY
         // 
         this.labelLightY.AutoSize = true;
         this.labelLightY.Location = new System.Drawing.Point(149, 22);
         this.labelLightY.Name = "labelLightY";
         this.labelLightY.Size = new System.Drawing.Size(47, 13);
         this.labelLightY.TabIndex = 4;
         this.labelLightY.Text = "Y = 10.0";
         // 
         // labelLightX
         // 
         this.labelLightX.AutoSize = true;
         this.labelLightX.Location = new System.Drawing.Point(22, 22);
         this.labelLightX.Name = "labelLightX";
         this.labelLightX.Size = new System.Drawing.Size(47, 13);
         this.labelLightX.TabIndex = 3;
         this.labelLightX.Text = "X = 10.0";
         // 
         // trackBarLightZ
         // 
         this.trackBarLightZ.LargeChange = 20;
         this.trackBarLightZ.Location = new System.Drawing.Point(283, 49);
         this.trackBarLightZ.Maximum = 250;
         this.trackBarLightZ.Minimum = -250;
         this.trackBarLightZ.Name = "trackBarLightZ";
         this.trackBarLightZ.Size = new System.Drawing.Size(125, 45);
         this.trackBarLightZ.TabIndex = 6;
         this.trackBarLightZ.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarLightZ.Value = 100;
         this.trackBarLightZ.ValueChanged += new System.EventHandler(this.trackBarLightZ_ValueChanged);
         // 
         // trackBarLightY
         // 
         this.trackBarLightY.LargeChange = 20;
         this.trackBarLightY.Location = new System.Drawing.Point(152, 49);
         this.trackBarLightY.Maximum = 250;
         this.trackBarLightY.Minimum = -250;
         this.trackBarLightY.Name = "trackBarLightY";
         this.trackBarLightY.Size = new System.Drawing.Size(125, 45);
         this.trackBarLightY.TabIndex = 5;
         this.trackBarLightY.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarLightY.Value = 100;
         this.trackBarLightY.ValueChanged += new System.EventHandler(this.trackBarLightY_ValueChanged);
         // 
         // trackBarLightX
         // 
         this.trackBarLightX.LargeChange = 20;
         this.trackBarLightX.Location = new System.Drawing.Point(25, 49);
         this.trackBarLightX.Maximum = 250;
         this.trackBarLightX.Minimum = -250;
         this.trackBarLightX.Name = "trackBarLightX";
         this.trackBarLightX.Size = new System.Drawing.Size(125, 45);
         this.trackBarLightX.TabIndex = 4;
         this.trackBarLightX.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarLightX.Value = 100;
         this.trackBarLightX.ValueChanged += new System.EventHandler(this.trackBarLightX_ValueChanged);
         // 
         // comboBoxLightColor
         // 
         this.comboBoxLightColor.AllowDrop = true;
         this.comboBoxLightColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.comboBoxLightColor.FormattingEnabled = true;
         this.comboBoxLightColor.Items.AddRange(new object[] {
            "Black",
            "Blue",
            "Green",
            "Magenta",
            "Pink",
            "Purple",
            "Red",
            "White",
            "Yellow"});
         this.comboBoxLightColor.Location = new System.Drawing.Point(21, 58);
         this.comboBoxLightColor.Name = "comboBoxLightColor";
         this.comboBoxLightColor.Size = new System.Drawing.Size(121, 21);
         this.comboBoxLightColor.Sorted = true;
         this.comboBoxLightColor.TabIndex = 8;
         this.comboBoxLightColor.SelectedIndexChanged += new System.EventHandler(this.comboBoxLightColor_SelectedIndexChanged);
         // 
         // colorlabel
         // 
         this.colorlabel.AutoSize = true;
         this.colorlabel.Location = new System.Drawing.Point(21, 36);
         this.colorlabel.Name = "colorlabel";
         this.colorlabel.Size = new System.Drawing.Size(114, 13);
         this.colorlabel.TabIndex = 14;
         this.colorlabel.Text = "Point light source color";
         // 
         // trackBarAmbient
         // 
         this.trackBarAmbient.Location = new System.Drawing.Point(21, 159);
         this.trackBarAmbient.Maximum = 100;
         this.trackBarAmbient.Name = "trackBarAmbient";
         this.trackBarAmbient.Size = new System.Drawing.Size(125, 45);
         this.trackBarAmbient.TabIndex = 9;
         this.trackBarAmbient.TickStyle = System.Windows.Forms.TickStyle.None;
         this.trackBarAmbient.Value = 25;
         this.trackBarAmbient.ValueChanged += new System.EventHandler(this.trackBarAmbient_ValueChanged);
         // 
         // labelAmbient
         // 
         this.labelAmbient.AutoSize = true;
         this.labelAmbient.Location = new System.Drawing.Point(18, 136);
         this.labelAmbient.Name = "labelAmbient";
         this.labelAmbient.Size = new System.Drawing.Size(100, 13);
         this.labelAmbient.TabIndex = 16;
         this.labelAmbient.Text = "Ambient light = 0.25";
         // 
         // groupBox3
         // 
         this.groupBox3.Controls.Add(this.labelAmbient);
         this.groupBox3.Controls.Add(this.trackBarAmbient);
         this.groupBox3.Controls.Add(this.colorlabel);
         this.groupBox3.Controls.Add(this.comboBoxLightColor);
         this.groupBox3.Location = new System.Drawing.Point(526, 43);
         this.groupBox3.Name = "groupBox3";
         this.groupBox3.Size = new System.Drawing.Size(179, 210);
         this.groupBox3.TabIndex = 17;
         this.groupBox3.TabStop = false;
         this.groupBox3.Text = "Light Attributes";
         // 
         // groupBox4
         // 
         this.groupBox4.Controls.Add(this.trackBarTimer);
         this.groupBox4.Controls.Add(this.labelTimer);
         this.groupBox4.Location = new System.Drawing.Point(526, 267);
         this.groupBox4.Name = "groupBox4";
         this.groupBox4.Size = new System.Drawing.Size(179, 100);
         this.groupBox4.TabIndex = 18;
         this.groupBox4.TabStop = false;
         this.groupBox4.Text = "Movement Attributes";
         // 
         // MainForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(721, 779);
         this.Controls.Add(this.groupBox4);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.glControl1);
         this.Controls.Add(this.menuStrip1);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.groupBox3);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MainMenuStrip = this.menuStrip1;
         this.MaximizeBox = false;
         this.Name = "MainForm";
         this.Text = "3D Viewer";
         this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
         this.Load += new System.EventHandler(this.MainForm_Load);
         this.Shown += new System.EventHandler(this.MainForm_Shown);
         this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarEyeX)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarEyeY)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarEyeZ)).EndInit();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarTimer)).EndInit();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarLightZ)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarLightY)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarLightX)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarAmbient)).EndInit();
         this.groupBox3.ResumeLayout(false);
         this.groupBox3.PerformLayout();
         this.groupBox4.ResumeLayout(false);
         this.groupBox4.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileLoadFigures;
        private System.Windows.Forms.ToolStripMenuItem menuFileExit;
        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.TrackBar trackBarEyeX;
        private System.Windows.Forms.TrackBar trackBarEyeY;
        private System.Windows.Forms.TrackBar trackBarEyeZ;
        private System.Windows.Forms.Label labelEyeX;
        private System.Windows.Forms.Label labelEyeY;
        private System.Windows.Forms.Label labelEyeZ;
        private System.Windows.Forms.Button btnEyeReset;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TrackBar trackBarTimer;
        private System.Windows.Forms.Label labelTimer;
        private System.Windows.Forms.Timer movementTimer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TrackBar trackBarLightZ;
        private System.Windows.Forms.TrackBar trackBarLightY;
        private System.Windows.Forms.TrackBar trackBarLightX;
        private System.Windows.Forms.ComboBox comboBoxLightColor;
        private System.Windows.Forms.Label colorlabel;
        private System.Windows.Forms.TrackBar trackBarAmbient;
        private System.Windows.Forms.Label labelAmbient;
        private System.Windows.Forms.Label labelLightZ;
        private System.Windows.Forms.Label labelLightY;
        private System.Windows.Forms.Label labelLightX;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.Button btnLightReset;
      private System.Windows.Forms.GroupBox groupBox4;
   }
}


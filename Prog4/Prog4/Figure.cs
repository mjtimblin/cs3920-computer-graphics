﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file contains code to create and show a "figure."
 */

using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.IO;

/// <summary>
/// This class creates and draws a "figure."
/// </summary>
class Figure
{
   private readonly float HALF = 0.5f;
   private readonly float DEGREE_TO_RADIAN_FACTOR = 0.01745329251f; // pi/180

   private string name;
   private VertexData[] verts;
   private int vboHandle;
   private int vaoHandle;
   private Vector3 centerPoint;
   private Matrix4 displayMatrix;
   private Vector3 max;
   private Vector3 min;
   private Vector3 translateAmount;

   /// <summary>
   /// This method creates a vertex buffer object (vboHandle) and a vertex 
   /// array object (vaoHandle) from an array of VertexData obtained from 
   /// the provided VertexDataList.
   /// Preconditions:
   ///    1. list is not empty.
   /// </summary>
   /// <param name="list"></param>
   public void Load(VertexDataList list, float shininess)
   {
      Shininess = shininess;
      verts = list.VertexArray();

      min = max = verts[0].Position;

      foreach (VertexData element in verts)
      {
         if (element.Position.X < min.X)
            min = new Vector3(element.Position.X, min.Y, min.Z);
         if (element.Position.Y < min.Y)
            min = new Vector3(min.X, element.Position.Y, min.Z);
         if (element.Position.Z < min.Z)
            min = new Vector3(min.X, min.Y, element.Position.Z);
         if (element.Position.X > max.X)
            max = new Vector3(element.Position.X, max.Y, max.Z);
         if (element.Position.Y > max.Y)
            max = new Vector3(max.X, element.Position.Y, max.Z);
         if (element.Position.Z > max.Z)
            max = new Vector3(max.X, max.Y, element.Position.Z);
      }

      centerPoint = new Vector3((min.X + max.X) * HALF,
         (min.Y + max.Y) * HALF, (min.Z + max.Z) * HALF);

      translateAmount = centerPoint;
      displayMatrix = Matrix4.CreateTranslation(-1 * centerPoint);

      GL.GenBuffers(1, out vboHandle);
      GL.BindBuffer(BufferTarget.ArrayBuffer, vboHandle);
      GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(verts.Length *
         BlittableValueType.StrideOf(verts)), verts,
         BufferUsageHint.StaticDraw);

      GL.GenVertexArrays(1, out vaoHandle);
      GL.BindVertexArray(vaoHandle);

      int vertPosLoc = GL.GetAttribLocation
         (ShaderLoader.Instance.ProgramHandle, "VertexPosition");
      int vertNormalLoc = GL.GetAttribLocation
         (ShaderLoader.Instance.ProgramHandle, "VertexNormal");
      int vertColorLoc = GL.GetAttribLocation
         (ShaderLoader.Instance.ProgramHandle, "VertexColor");

      GL.EnableVertexAttribArray(vertPosLoc);
      GL.EnableVertexAttribArray(vertColorLoc);
      GL.EnableVertexAttribArray(vertNormalLoc);

      GL.VertexAttribPointer(vertPosLoc, 3, VertexAttribPointerType.Float,
         false, BlittableValueType.StrideOf(verts), (IntPtr)0);
      GL.VertexAttribPointer(vertColorLoc, 3, VertexAttribPointerType.Float,
         false, BlittableValueType.StrideOf(verts), (IntPtr)12);
      GL.VertexAttribPointer(vertNormalLoc, 3, VertexAttribPointerType.Float,
         false, BlittableValueType.StrideOf(verts), (IntPtr)24);
      GL.BindVertexArray(0);
   }

   /// <summary>
   /// This method uses the vaoHandle to draw the "figure."
   /// </summary>
   public void Show(Matrix4 lookAt)
   {
      GL.BindVertexArray(vaoHandle);

      Matrix4 mat = displayMatrix * Matrix4.CreateTranslation
         (translateAmount);
      int modelMatrixUniformLocation = GL.GetUniformLocation
         (ShaderLoader.Instance.ProgramHandle, "ModelMatrix");
      GL.UniformMatrix4(modelMatrixUniformLocation, false, ref mat);

      Matrix4 normalMatrix = mat * lookAt;
      normalMatrix.Transpose();
      normalMatrix.Invert();
      int normalMatrixUniformLocation = GL.GetUniformLocation
         (ShaderLoader.Instance.ProgramHandle, "NormalMatrix");
      GL.UniformMatrix4(normalMatrixUniformLocation, false,
         ref normalMatrix);

      float shininess = Shininess;
      int shininessUniformLocation = GL.GetUniformLocation
         (ShaderLoader.Instance.ProgramHandle, "Shininess");
      GL.Uniform1(shininessUniformLocation, shininess);

      GL.DrawArrays(PrimitiveType.Triangles, 0, verts.Length);
      GL.BindVertexArray(0);
   }

   /// <summary>
   /// This method rotates the figure around the given axis based on the
   /// given degrees.
   /// </summary>
   /// <param name="axis"></param>
   /// <param name="degrees"></param>
   public void Rotate(Vector3 axis, float degrees)
   {
      float angle = DEGREE_TO_RADIAN_FACTOR * degrees;
      displayMatrix = displayMatrix *
          Matrix4.CreateTranslation(-1 * centerPoint) *
          Matrix4.CreateFromAxisAngle(axis, angle) *
          Matrix4.CreateTranslation(centerPoint);
   }

   /// <summary>
   /// This method scales the figure based on the given x, y, and z values.
   /// </summary>
   /// <param name="x"></param>
   /// <param name="y"></param>
   /// <param name="z"></param>
   public void Scale(float x, float y, float z)
   {
      displayMatrix = displayMatrix *
          Matrix4.CreateTranslation(-1 * centerPoint) *
          Matrix4.CreateScale(x, y, z) *
          Matrix4.CreateTranslation(centerPoint);
   }

   /// <summary>
   /// This method translates the figure based on the given x, y, z values.
   /// </summary>
   /// <param name="x"></param>
   /// <param name="y"></param>
   /// <param name="z"></param>
   public void Translate(float x, float y, float z)
   {
      translateAmount = new Vector3(translateAmount.X + x,
          translateAmount.Y + y, translateAmount.Z + z);
   }

   /// <summary>
   /// This method sets displayMatix and translateAmout to their initial
   /// values to reset the figure's transformations.
   /// </summary>
   public void Restore()
   {
      translateAmount = centerPoint;
      displayMatrix = Matrix4.CreateTranslation(-1 * centerPoint);
   }

   /// <summary>
   /// Getter: Returns name
   /// Setter: Sets name to a provided filename without the ".wrl" extension
   /// </summary>
   public string Name
   {
      get { return name; }
      set
      {
         name = new FileInfo(value).Name;
         name = name.Substring(0, name.IndexOf(".wrl"));
      }
   }

   /// <summary>
   /// This method returns the current center of figure based based on the
   /// starting center point and the translateAmount.
   /// </summary>
   public Vector3 CurrentCenter
   {
      get
      {
         return centerPoint + translateAmount;
      }
   }

   /// <summary>
   /// Getter: Returns Shininess
   /// Setter: Sets Shininess to a given float
   /// </summary>
   public float Shininess { get; set; }
}
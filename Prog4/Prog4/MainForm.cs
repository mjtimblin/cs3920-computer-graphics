﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This is the main form for displaying and interfacing with OpenGL
 *          and OpenTK to display 3D models
 */

using System;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Drawing;

namespace Prog4
{
   /// <summary>
   /// Main form initializing components and classes
   /// </summary>
   public partial class MainForm : Form
   {
      private readonly float WINDOW_SIZE = 1.5f;
      private readonly float WIN_NEAR = 2.0f;
      private readonly float WIN_FAR = 80.0f;
      private readonly int MAX_TIMER_DURATION = 2000;
      private readonly float TRACKBAR_POSITION_RATIO = 10.0f;
      private readonly float TRACKBAR_AMBIENT_RATIO = 100.0f;
      private readonly int TRACKBAR_EYE_STARTING_VALUE = 50;
      private readonly int TRACKBAR_LIGHT_STARTING_VALUE = 100;
      private readonly float STARTING_EYE_POSITION = 5.0f;
      private readonly float STARTING_LIGHT_POSITION = 10.0f;
      private readonly float STARTING_LIGHT_LEVEL = 0.25f;
      private readonly int WHITE_INDEX = 7;
      private readonly Vector3[] COLOR_ARRAY = {
         new Vector3(0.0f, 0.0f, 0.0f),     // Black
         new Vector3(0.0f, 0.0f, 1.0f),     // Blue
         new Vector3(0.0f, 1.0f, 0.0f),     // Green
         new Vector3(0.81f, 0.25f, 0.49f),  // Magenta
         new Vector3(1.0f, 0.75f, 0.79f),   // Pink
         new Vector3(0.62f, 0.12f, 0.94f),  // Purple
         new Vector3(1.0f, 0.0f, 0.0f),     // Red
         new Vector3(1.0f, 1.0f, 1.0f),     // White
         new Vector3(1.0f, 1.0f, 0.0f)      // Yellow
      };

      private FigureList figList = new FigureList();
      private float ambient;
      private Vector3 eye;
      private Vector3 lightPosition;
      private Vector3 lightColor;

      /// <summary>
      /// Constructor: This initaializes graphical elements and initializes
      /// eye to its starting position.
      /// </summary>
      public MainForm()
      {
         InitializeComponent();
         eye = new Vector3(STARTING_EYE_POSITION, STARTING_EYE_POSITION,
            STARTING_EYE_POSITION);
         lightPosition = new Vector3(STARTING_LIGHT_POSITION,
            STARTING_LIGHT_POSITION, STARTING_LIGHT_POSITION);
         ambient = STARTING_LIGHT_LEVEL;
         lightColor = COLOR_ARRAY[WHITE_INDEX];
      }

      /// <summary>
      /// Calls OpenGL methods to allow orthographic view on load event
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_Load(object sender, EventArgs e)
      {
         ShaderLoader.Instance.Load(Directory.GetCurrentDirectory() +
            "\\Prog4_VS.glsl", Directory.GetCurrentDirectory() +
            "\\Prog4_FS.glsl");
         if (!ShaderLoader.Instance.LastLoadError.Equals(""))
            MessageBox.Show(ShaderLoader.Instance.LastLoadError, "3D Viewer",
               MessageBoxButtons.OK, MessageBoxIcon.Warning);

         GL.Enable(EnableCap.DepthTest);
         float mult = (float)glControl1.Height / (float)glControl1.Width;
         Matrix4 projMat = Matrix4.CreatePerspectiveOffCenter(
             -WINDOW_SIZE, WINDOW_SIZE, -WINDOW_SIZE * mult,
             WINDOW_SIZE * mult, WIN_NEAR, WIN_FAR);

         int projectionMatrixUniformLocation =
            GL.GetUniformLocation(ShaderLoader.Instance.ProgramHandle,
            "ProjectionMatrix");
         GL.UniformMatrix4(projectionMatrixUniformLocation, false,
            ref projMat);
         ShowFigures();
         comboBoxLightColor.SelectedIndex = WHITE_INDEX;
      }

      /// <summary>
      /// shows axes for initial load of program
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_Shown(object sender, EventArgs e)
      {
         ShowFigures();
      }

      /// <summary>
      /// shows figure when size of window is changed
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_SizeChanged(object sender, EventArgs e)
      {
         glControl1.SwapBuffers();
      }

      /// <summary>
      /// This method is called when the form is closing, and it unloads
      /// the shaders.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
      {
         ShaderLoader.Instance.Unload();
      }

      /// <summary>
      /// Shows the 3D figure
      /// </summary>
      private void ShowFigures()
      {
         GL.Clear(ClearBufferMask.ColorBufferBit |
             ClearBufferMask.DepthBufferBit);
         Matrix4 lookAt = Matrix4.LookAt(eye.X, eye.Y, eye.Z, 0.0f, 0.0f,
             0.0f, 0.0f, 1.0f, 0.0f);

         int globalAmbientUniformLocation =
             GL.GetUniformLocation(ShaderLoader.Instance.ProgramHandle,
             "GlobalAmbient");
         GL.Uniform1(globalAmbientUniformLocation, ambient);

         int lightPositionUniformLocation =
             GL.GetUniformLocation(ShaderLoader.Instance.ProgramHandle,
             "LightPosition");
         GL.Uniform3(lightPositionUniformLocation, ref lightPosition);

         int lightColorUniformLocation =
             GL.GetUniformLocation(ShaderLoader.Instance.ProgramHandle,
             "LightColor");
         GL.Uniform3(lightColorUniformLocation, ref lightColor);

         int viewMatrixUniformLocation =
             GL.GetUniformLocation(ShaderLoader.Instance.ProgramHandle,
             "ViewMatrix");
         GL.UniformMatrix4(viewMatrixUniformLocation, false, ref lookAt);

         Axes.Instance.Show(lookAt);
         figList.Show(lookAt);

         glControl1.SwapBuffers();
      }

      /// <summary>
      /// This method is called when the LoadFigures menu item is selected.
      /// It opens a FolderBrowserDialog and calls figList.LoadFigures on the
      /// chosen folder.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void menuFileLoadFigures_Click(object sender, EventArgs e)
      {
         FolderBrowserDialog folderBrowserDialog1 =
                new FolderBrowserDialog();
         folderBrowserDialog1.SelectedPath =
            "K:\\Courses\\CSSE\\tianb\\cs3920_cs5920\\jon_tim\\Prog4\\" + 
            "figures";
         if (folderBrowserDialog1.ShowDialog().ToString() != "Cancel")
            figList.LoadFigures(folderBrowserDialog1.SelectedPath);
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the Exit menu item is selected. It simply
      /// exits the program.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void menuFileExit_Click(object sender, EventArgs e)
      {
         Application.Exit();
      }

      /// <summary>
      /// This method is called when the btnEyeReset button is clicked. The
      /// method resets the eye to the starting position.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnEyeReset_Click(object sender, EventArgs e)
      {
         eye = new Vector3(STARTING_EYE_POSITION, STARTING_EYE_POSITION,
            STARTING_EYE_POSITION);
         trackBarEyeX.Value = TRACKBAR_EYE_STARTING_VALUE;
         trackBarEyeY.Value = TRACKBAR_EYE_STARTING_VALUE;
         trackBarEyeZ.Value = TRACKBAR_EYE_STARTING_VALUE;
         labelEyeX.Text = "X = " + STARTING_EYE_POSITION.ToString("0.0");
         labelEyeY.Text = "Y = " + STARTING_EYE_POSITION.ToString("0.0");
         labelEyeZ.Text = "Z = " + STARTING_EYE_POSITION.ToString("0.0");
         figList.Restore();
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the btnLightReset button is clicked. The
      /// method resets the point light source to the starting position.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnLightReset_Click(object sender, EventArgs e)
      {
         lightPosition = new Vector3(STARTING_LIGHT_POSITION,
            STARTING_LIGHT_POSITION, STARTING_LIGHT_POSITION);
         trackBarLightX.Value = TRACKBAR_LIGHT_STARTING_VALUE;
         trackBarLightY.Value = TRACKBAR_LIGHT_STARTING_VALUE;
         trackBarLightZ.Value = TRACKBAR_LIGHT_STARTING_VALUE;
         labelLightX.Text = "X = " + STARTING_LIGHT_POSITION.ToString("0.0");
         labelLightY.Text = "Y = " + STARTING_LIGHT_POSITION.ToString("0.0");
         labelLightZ.Text = "Z = " + STARTING_LIGHT_POSITION.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the selected item of comboBoxLightColor
      /// changes. It updates the lightColor with the color in COLOR_ARRAY 
      /// corresponding to the selected index.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void comboBoxLightColor_SelectedIndexChanged(object sender,
         EventArgs e)
      {
         lightColor = COLOR_ARRAY[comboBoxLightColor.SelectedIndex];
         ShowFigures();
      }

      /// <summary>
      /// This method is called when movementTimer ticks. It calls the show
      /// method on figList and calls the ShowFigures.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void movementTimer_Tick(object sender, EventArgs e)
      {
         figList.Move();
         ShowFigures();
      }

      /// <summary>
      /// scales time between movement based on a slider
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarTimer_ValueChanged(object sender, EventArgs e)
      {
         if (trackBarTimer.Value == 0)
            movementTimer.Enabled = false;
         else
         {
            movementTimer.Interval = MAX_TIMER_DURATION /
                trackBarTimer.Value;
            movementTimer.Enabled = true;
         }
         labelTimer.Text = "Speed = " +
             trackBarTimer.Value + "%";
      }

      /// <summary>
      /// This method is called when the value of trackBarAmbient changes.
      /// It updates the value of ambient and calls ShowFigures.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarAmbient_ValueChanged(object sender, EventArgs e)
      {
         float ambientValue = trackBarAmbient.Value / TRACKBAR_AMBIENT_RATIO;
         ambient = ambientValue;
         labelAmbient.Text = "Ambient light = " + ambientValue;
         ShowFigures();
      }

      /// <summary>
      /// changes viewing handle of x-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarX_ValueChanged(object sender, EventArgs e)
      {
         float xPosition = trackBarEyeX.Value / TRACKBAR_POSITION_RATIO;
         eye = new Vector3(xPosition, eye.Y, eye.Z);
         labelEyeX.Text = "X = " + xPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// changes viewing handle of y-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarY_ValueChanged(object sender, EventArgs e)
      {
         float yPosition = trackBarEyeY.Value / TRACKBAR_POSITION_RATIO;
         eye = new Vector3(eye.X, yPosition, eye.Z);
         labelEyeY.Text = "Y = " + yPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// changes viewing handle of z-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarZ_ValueChanged(object sender, EventArgs e)
      {
         float zPosition = trackBarEyeZ.Value / TRACKBAR_POSITION_RATIO;
         eye = new Vector3(eye.X, eye.Y, zPosition);
         labelEyeZ.Text = "Z = " + zPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the value of trackBarLightX changes.
      /// It updates the X value of lightPosition and calls ShowFigures.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarLightX_ValueChanged(object sender, EventArgs e)
      {
         float xPosition = trackBarLightX.Value / TRACKBAR_POSITION_RATIO;
         lightPosition = new Vector3(xPosition, lightPosition.Y,
            lightPosition.Z);
         labelLightX.Text = "X = " + xPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the value of trackBarLightY changes.
      /// It updates the Y value of lightPosition and calls ShowFigures.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarLightY_ValueChanged(object sender, EventArgs e)
      {
         float yPosition = trackBarLightY.Value / TRACKBAR_POSITION_RATIO;
         lightPosition = new Vector3(lightPosition.Y, yPosition,
            lightPosition.Z);
         labelLightY.Text = "Y = " + yPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the value of trackBarLightZ changes.
      /// It updates the Z value of lightPosition and calls ShowFigures.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarLightZ_ValueChanged(object sender, EventArgs e)
      {
         float zPosition = trackBarLightZ.Value / TRACKBAR_POSITION_RATIO;
         lightPosition = new Vector3(lightPosition.X, lightPosition.Y,
            zPosition);
         labelLightZ.Text = "Z = " + zPosition.ToString("0.0");
         ShowFigures();
      }
   }
}
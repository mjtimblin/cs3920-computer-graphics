﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file contains varaiables and methods necissary for holding
 *          and interacting with a list of figures loaded from a given
 *          directory.
 */

using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;

namespace Prog4
{
   /// <summary>
   /// figure list class is for holding a list of figures
   /// </summary>
   class FigureList
   {
      /// <summary>
      /// struct holds the glue that connects figure class and its connected
      /// movement pattern
      /// </summary>
      private struct FigureMovementPair
      {
         public Figure fig;
         public MovePattern movement;
      }

      private const int MAX_NUM_PATTERNS = 4;
      private const int PATTERN_ONE = 0;
      private const int PATTERN_TWO = 1;
      private const int PATTERN_THREE = 2;
      private const int PATTERN_FOUR = 3;
      private const float HALF = 0.5f;
      private const int MINIMUM_SHINY_FIGURES = 2;
      private const float MINIMUM_SHININESS = 20f;
      private const float MAXIMUM_SHININESS = 250f;

      private List<FigureMovementPair> figList =
          new List<FigureMovementPair>();
      private Random random = new Random();

      /// <summary>
      /// Load figures from file and puts them in a list, associates movement
      /// to them in sequence
      /// </summary>
      /// <param name="folderName"></param>
      public void LoadFigures(string folderName)
      {
         int patNum = PATTERN_ONE;
         string[] files = Directory.GetFiles(folderName);
         int numFigures = 0;
         int numShinyFigures = 0;
         foreach (string filePath in files)
         {
            if (filePath.EndsWith(".wrl"))
               numFigures++;
         }
         foreach (string filePath in files)
         {
            if (filePath.EndsWith(".wrl"))
            {
               VertexDataList list = new VertexDataList();
               if (list.LoadDataFromVRML(filePath))
               {
                  Figure fig = new Figure();
                  if (numShinyFigures < MINIMUM_SHINY_FIGURES || 
                     numShinyFigures < numFigures * HALF)
                  {
                     fig.Load(list, (float)random.NextDouble() *
                        (MAXIMUM_SHININESS - MINIMUM_SHININESS) +
                        MINIMUM_SHININESS);
                  }
                  else
                  {
                     fig.Load(list, 0.0f);
                  }
                  fig.Name = filePath;
                  FigureMovementPair newPair = new FigureMovementPair();
                  newPair.fig = fig;
                  switch (patNum)
                  {
                     case PATTERN_ONE:
                        newPair.movement =
                            new LateralusMovePattern();
                        break;
                     case PATTERN_TWO:
                        newPair.movement =
                            new StretchContractMovePattern();
                        break;
                     case PATTERN_THREE:
                        newPair.movement =
                            new TopMovePattern();
                        break;
                     case PATTERN_FOUR:
                        newPair.movement =
                            new ConeMovePattern(newPair.fig);
                        break;
                  }
                  patNum = (patNum + 1) % MAX_NUM_PATTERNS;
                  figList.Add(newPair);
               }
            }
         }
      }

      /// <summary>
      /// This method calls the show method on the figure in each
      /// FigureMovementPair in figList.
      /// </summary>
      public void Show(Matrix4 lookAt)
      {
         foreach (FigureMovementPair pair in figList)
         {
            pair.fig.Show(lookAt);
         }
      }

      /// <summary>
      /// This method calls the move method on the movement in each
      /// FigureMovementPair in figList.
      /// </summary>
      public void Move()
      {
         foreach (FigureMovementPair pair in figList)
         {
            pair.movement.Move(pair.fig);
         }
      }

      /// <summary>
      /// This method calls the restore method on the figure in each
      /// FigureMovementPair in figList.
      /// </summary>
      public void Restore()
      {
         foreach (FigureMovementPair pair in figList)
         {
            pair.fig.Restore();
         }
      }
   }
}

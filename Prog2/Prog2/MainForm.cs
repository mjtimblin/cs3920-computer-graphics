﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This is the main form for displaying and interfacing with OPENGL
 *          to display 3D models
 */

using System;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Prog2
{
   /// <summary>
   /// Main form initializing components and classes
   /// </summary>
   public partial class MainForm : Form
   {
      private readonly float startingPosition = 5.0f;
      private Figure fig;
      private Vector3 eye;

      /// <summary>
      /// called on loaded loaded, calls initialize method and vector class
      /// </summary>
      public MainForm()
      {
         InitializeComponent();
         eye = new Vector3(startingPosition, startingPosition,
            startingPosition);
      }

      /// <summary>
      /// event handle to handle menu functions
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void menuFileOpen_Click(object sender, EventArgs e)
      {
         OpenFileDialog openFileDialog1 = new OpenFileDialog();
         openFileDialog1.Filter = "VMRL files|*.wrl";
         openFileDialog1.Title = "Open a VMRL File";
         openFileDialog1.ShowDialog();
         if (openFileDialog1.FileName != "")
         {
            VertexDataList list = new VertexDataList();
            if (list.LoadDataFromVRML(openFileDialog1.FileName))
            {
               fig = new Figure();
               fig.Load(list);
               fig.Name = openFileDialog1.FileName;
            }
         }
         ShowFigure();
      }

      /// <summary>
      /// event handler for exiting program from menu
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void menuFileExit_Click(object sender, EventArgs e)
      {
         Application.Exit();
      }

      /// <summary>
      /// calls open gl methods to allow orthographic view on load event
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_Load(object sender, EventArgs e)
      {
         GL.Enable(EnableCap.DepthTest);
         GL.MatrixMode(MatrixMode.Projection);
         Matrix4 projMat = Matrix4.CreateOrthographic(10.0f, 10.0f, 0.5f,
            100.0f);
         GL.LoadMatrix(ref projMat);
         ShowFigure();
      }

      /// <summary>
      /// shows 3D figure
      /// </summary>
      private void ShowFigure()
      {
         GL.Clear(ClearBufferMask.ColorBufferBit |
             ClearBufferMask.DepthBufferBit);
         if (fig != null)
         {
            fig.Show();
            this.Text = "3D Viewer          Current Figure: " + fig.Name;
         }
         else
         {
            this.Text = "3D Viewer          Current Figure: none";
         }
         Axes.Instance.Show();
         Matrix4 lookAt = Matrix4.LookAt(eye.X, eye.Y, eye.Z, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f);
         GL.MatrixMode(MatrixMode.Modelview);
         GL.LoadMatrix(ref lookAt);
         glControl1.SwapBuffers();
      }

      /// <summary>
      /// shows axes for initial load of program
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_Shown(object sender, EventArgs e)
      {
         ShowFigure();
      }

      /// <summary>
      /// shows figure when size of window is changed
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_SizeChanged(object sender, EventArgs e)
      {
         ShowFigure();
      }

      /// <summary>
      /// changes viewing handle of x-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarX_ValueChanged(object sender, EventArgs e)
      {
         eye = new Vector3(trackBarX.Value, eye.Y, eye.Z);
         labelX.Text = "X = " + trackBarX.Value;
         ShowFigure();
      }

      /// <summary>
      /// changes viewing handle of y-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarY_ValueChanged(object sender, EventArgs e)
      {
         eye = new Vector3(eye.X, trackBarY.Value, eye.Z);
         labelY.Text = "Y = " + trackBarY.Value;
         ShowFigure();
      }

      /// <summary>
      /// changes viewing handle of z-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarZ_ValueChanged(object sender, EventArgs e)
      {
         eye = new Vector3(eye.X, eye.Y, trackBarZ.Value);
         labelZ.Text = "Z = " + trackBarZ.Value;
         ShowFigure();
      }

      /// <summary>
      /// This method is called when the btnReset button is clicked. The
      /// method resets the eye to the starting position.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnReset_Click(object sender, EventArgs e)
      {
         eye = new Vector3(startingPosition, startingPosition,
            startingPosition);
         trackBarX.Value = (int)startingPosition;
         trackBarY.Value = (int)startingPosition;
         trackBarZ.Value = (int)startingPosition;
         labelX.Text = "X = 5";
         labelY.Text = "Y = 5";
         labelZ.Text = "Z = 5";
         ShowFigure();
      }
   }
}
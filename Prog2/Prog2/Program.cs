﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file contains autogenerated code that serves as the entry
 *          point for this program.
 */

using System;
using System.Windows.Forms;

namespace Prog2
{
   /// <summary>
   /// Autogenerated class. Contains main entry point for the application.
   /// </summary>
   static class Program
   {
      /// <summary>
      /// Autogenerated method. The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main()
      {
         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);
         Application.Run(new MainForm());
      }
   }
}

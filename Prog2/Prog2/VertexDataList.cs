/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file contains code to read and parse a .VMRL file. It also
 *          contains a struct to manage vertex information. 
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;

/// <summary>
/// This struct stores vertex information
/// </summary>
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct VertexData
{
   public Vector3 Position;
   public Vector3 Color;
   public Vector3 Normal;

   public VertexData(Vector3 pos, Vector3 col, Vector3 norm)
   {
      Position = pos;
      Color = col;
      Normal = norm;
   }

   public VertexData(Vector3 pos, Vector3 col)
   {
      Position = pos;
      Color = col;
      Normal = new Vector3(0.0F, 0.0F, 0.0F);
   }
}

/// <summary>
/// This class reads and parses a .VMRL file.
/// Preconditions: 
///    1. VMRL is good, and everything is decomposed into triangles.
/// </summary>
public class VertexDataList
{
   private List<VertexData> vertList = new List<VertexData>(100);
   private StreamReader infile;

   /// <summary>
   /// This method reads and parses a .VMRL file obtained from the given 
   /// filename.
   /// </summary>
   /// <param name="fullPathFileName"></param>
   /// <returns></returns>
   public bool LoadDataFromVRML(string fullPathFileName)
   {
      vertList.Clear();
      try
      {
         infile = new StreamReader(fullPathFileName);
         if (infile == null)
            return false;

         bool done = false;
         while (!done)
         {
            if (!LookForInVMRL("coord Coordinate { point ["))
               done = true;
            else
            {
               ReadVertsForShapeInVRML();
            }
         }

         if (vertList.Count == 0)
            return false;
         return true;
      }
      catch
      {
         // File not in the expected format
         if (infile != null)
            infile.Close();
         return false;
      }
   }

   /// <summary>
   /// This returns a VertexData array obtained by calling the ToArray method
   /// on vertList.
   /// </summary>
   /// <returns></returns>
   public VertexData[] VertexArray()
   {
      return vertList.ToArray();
   }


   /// <summary>
   /// This method calls methods to read and parse vertices from infile.
   /// Preconditions:
   ///    1. VMRL is good. If the file is bad, bad results will be returned, or an exception is thrown.
   ///    2. The read pointer is at the line after "coord Coordinate { point [" 
   /// </summary>
   private void ReadVertsForShapeInVRML()
   {
      List<Vector3> verts = new List<Vector3>();
      List<Vector3> colors = new List<Vector3>();
      List<ushort> vertIndices = new List<ushort>();
      List<ushort> colorIndices = new List<ushort>();

      // You fill this in to see if you understand this file and the XMRL file.
      // This should be exactly 7 lines of code, each one a call to a private method.

      ReadVectListFromVMRL(verts);
      LookForInVMRL("coordIndex [");
      ReadIndicesListFromVMRL(vertIndices);
      LookForInVMRL("color Color { color [");
      ReadVectListFromVMRL(colors);
      LookForInVMRL("colorIndex [");
      ReadIndicesListFromVMRL(colorIndices);

      for (int i = 0; i < vertIndices.Count; i++)
      {
         VertexData v = new VertexData(verts[vertIndices[i]], colors[colorIndices[i]]);
         vertList.Add(v);
      }

      CalculateNormals();
   }

   private void CalculateNormals()
   {
      // This will be completed in a future program
      // I will explain then why we don't want to use the normals the Wings3d exports.
      // This assumes that Wings3d exports faces in CC order.

      // After we cover this, you should be able to write this in a small number of lines using Vector3 operations.
      // Hint:  Loop through, processing 3 verts at a time.
      // Note that in C#, you can't do: vertList[i + 2].Normal = normal;
      // You need to do something like: vertList[i + 2] = new VertexData(vertList[i + 2].Position, vertList[i + 2].Color, normal);
   }

   /// <summary>
   /// This method reads the next line of infile until a given string is 
   /// found or EOF. If the line is found, true is returned. Otherwise, 
   /// false is returned. 
   /// </summary>
   /// <param name="s"></param>
   /// <returns></returns>
   private bool LookForInVMRL(string s)
   {
      string inLine;
      bool found = false;
      while (!found)
      {
         inLine = infile.ReadLine();
         if (inLine == null)     // returns null if EOF
            return false;
         if (inLine.IndexOf(s) >= 0)
            found = true;
      }
      return true;
   }

   /// <summary>
   /// This method reads and parses vectors from infile.
   /// </summary>
   /// <param name="vecList"></param>
   private void ReadVectListFromVMRL(List<Vector3> vecList)
   {
      string inLine;
      bool done = false;
      while (!done)
      {
         inLine = infile.ReadLine();
         Vector3 vect = new Vector3();
         string[] tokens = inLine.TrimStart().Split(new Char[] { ' ', ',' });
         for (int i = 0; i < 3; i++)
            vect[i] = float.Parse(tokens[i]);
         vecList.Add(vect);
         if (inLine.IndexOf(']') >= 0)
            done = true;
      }
   }

   /// <summary>
   /// This method reads and parses indices from infile.
   /// Preconditions:
   ///    1. There are 3 indices per line. We are assuming that everything
   ///       decomposed into triangles.
   /// </summary>
   /// <param name="indexList"></param>
   private void ReadIndicesListFromVMRL(List<ushort> indexList)
   {
      string inLine;
      bool done = false;
      while (!done)
      {
         inLine = infile.ReadLine();
         string[] tokens = inLine.TrimStart().Split(new Char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
         for (int i = 0; i < 3; i++)
            indexList.Add(ushort.Parse(tokens[i]));
         if (inLine.IndexOf(']') >= 0)
            done = true;
      }
   }
}
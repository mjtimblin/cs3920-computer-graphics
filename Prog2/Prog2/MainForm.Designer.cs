﻿namespace Prog2
{
   partial class MainForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
         this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
         this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
         this.glControl1 = new OpenTK.GLControl();
         this.trackBarX = new System.Windows.Forms.TrackBar();
         this.trackBarY = new System.Windows.Forms.TrackBar();
         this.trackBarZ = new System.Windows.Forms.TrackBar();
         this.labelX = new System.Windows.Forms.Label();
         this.labelY = new System.Windows.Forms.Label();
         this.labelZ = new System.Windows.Forms.Label();
         this.btnReset = new System.Windows.Forms.Button();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.menuStrip1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarX)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarY)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarZ)).BeginInit();
         this.groupBox1.SuspendLayout();
         this.SuspendLayout();
         // 
         // menuStrip1
         // 
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Size = new System.Drawing.Size(527, 24);
         this.menuStrip1.TabIndex = 0;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // menuFile
         // 
         this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileOpen,
            this.menuFileExit});
         this.menuFile.Name = "menuFile";
         this.menuFile.Size = new System.Drawing.Size(37, 20);
         this.menuFile.Text = "&File";
         // 
         // menuFileOpen
         // 
         this.menuFileOpen.Name = "menuFileOpen";
         this.menuFileOpen.Size = new System.Drawing.Size(103, 22);
         this.menuFileOpen.Text = "&Open";
         this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
         // 
         // menuFileExit
         // 
         this.menuFileExit.Name = "menuFileExit";
         this.menuFileExit.Size = new System.Drawing.Size(103, 22);
         this.menuFileExit.Text = "E&xit";
         this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
         // 
         // glControl1
         // 
         this.glControl1.BackColor = System.Drawing.Color.Black;
         this.glControl1.Location = new System.Drawing.Point(12, 174);
         this.glControl1.Name = "glControl1";
         this.glControl1.Size = new System.Drawing.Size(500, 500);
         this.glControl1.TabIndex = 1;
         this.glControl1.VSync = false;
         // 
         // trackBarX
         // 
         this.trackBarX.Location = new System.Drawing.Point(21, 46);
         this.trackBarX.Maximum = 25;
         this.trackBarX.Minimum = -25;
         this.trackBarX.Name = "trackBarX";
         this.trackBarX.Size = new System.Drawing.Size(125, 45);
         this.trackBarX.TabIndex = 2;
         this.trackBarX.Value = 5;
         this.trackBarX.ValueChanged += new System.EventHandler(this.trackBarX_ValueChanged);
         // 
         // trackBarY
         // 
         this.trackBarY.Location = new System.Drawing.Point(152, 46);
         this.trackBarY.Maximum = 25;
         this.trackBarY.Minimum = -25;
         this.trackBarY.Name = "trackBarY";
         this.trackBarY.Size = new System.Drawing.Size(125, 45);
         this.trackBarY.TabIndex = 3;
         this.trackBarY.Value = 5;
         this.trackBarY.ValueChanged += new System.EventHandler(this.trackBarY_ValueChanged);
         // 
         // trackBarZ
         // 
         this.trackBarZ.Location = new System.Drawing.Point(283, 46);
         this.trackBarZ.Maximum = 25;
         this.trackBarZ.Minimum = -25;
         this.trackBarZ.Name = "trackBarZ";
         this.trackBarZ.Size = new System.Drawing.Size(125, 45);
         this.trackBarZ.TabIndex = 4;
         this.trackBarZ.Value = 5;
         this.trackBarZ.ValueChanged += new System.EventHandler(this.trackBarZ_ValueChanged);
         // 
         // labelX
         // 
         this.labelX.AutoSize = true;
         this.labelX.Location = new System.Drawing.Point(22, 27);
         this.labelX.Name = "labelX";
         this.labelX.Size = new System.Drawing.Size(32, 13);
         this.labelX.TabIndex = 5;
         this.labelX.Text = "X = 5";
         // 
         // labelY
         // 
         this.labelY.AutoSize = true;
         this.labelY.Location = new System.Drawing.Point(149, 27);
         this.labelY.Name = "labelY";
         this.labelY.Size = new System.Drawing.Size(32, 13);
         this.labelY.TabIndex = 6;
         this.labelY.Text = "Y = 5";
         // 
         // labelZ
         // 
         this.labelZ.AutoSize = true;
         this.labelZ.Location = new System.Drawing.Point(280, 27);
         this.labelZ.Name = "labelZ";
         this.labelZ.Size = new System.Drawing.Size(32, 13);
         this.labelZ.TabIndex = 7;
         this.labelZ.Text = "Z = 5";
         // 
         // btnReset
         // 
         this.btnReset.Location = new System.Drawing.Point(414, 36);
         this.btnReset.Name = "btnReset";
         this.btnReset.Size = new System.Drawing.Size(75, 23);
         this.btnReset.TabIndex = 8;
         this.btnReset.Text = "Reset";
         this.btnReset.UseVisualStyleBackColor = true;
         this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.btnReset);
         this.groupBox1.Controls.Add(this.trackBarZ);
         this.groupBox1.Controls.Add(this.labelZ);
         this.groupBox1.Controls.Add(this.trackBarX);
         this.groupBox1.Controls.Add(this.labelY);
         this.groupBox1.Controls.Add(this.trackBarY);
         this.groupBox1.Controls.Add(this.labelX);
         this.groupBox1.Location = new System.Drawing.Point(12, 43);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(503, 99);
         this.groupBox1.TabIndex = 9;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "X, Y, Z values of the View Reference Point (Eyeball) looking at (0,0,0) with Y up" +
    "";
         // 
         // MainForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(527, 686);
         this.Controls.Add(this.glControl1);
         this.Controls.Add(this.menuStrip1);
         this.Controls.Add(this.groupBox1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MainMenuStrip = this.menuStrip1;
         this.MaximizeBox = false;
         this.Name = "MainForm";
         this.Text = "3D Viewer          Current Figure: none";
         this.Load += new System.EventHandler(this.MainForm_Load);
         this.Shown += new System.EventHandler(this.MainForm_Shown);
         this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarX)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarY)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.trackBarZ)).EndInit();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem menuFile;
      private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
      private System.Windows.Forms.ToolStripMenuItem menuFileExit;
      private OpenTK.GLControl glControl1;
        private System.Windows.Forms.TrackBar trackBarX;
        private System.Windows.Forms.TrackBar trackBarY;
        private System.Windows.Forms.TrackBar trackBarZ;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelZ;
        private System.Windows.Forms.Button btnReset;
      private System.Windows.Forms.GroupBox groupBox1;
   }
}


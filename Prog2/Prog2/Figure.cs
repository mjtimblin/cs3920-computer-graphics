﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file contains code to create and show a "figure."
 */

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.IO;

/// <summary>
/// This class creates and draws a "figure."
/// </summary>
class Figure
{
    private string name = null;
    private VertexData[] verts;
    private int vboHandle;
    private int vaoHandle;

    /// <summary>
    /// This method creates a vertex buffer object (vboHandle) and a vertex 
    /// array object (vaoHandle) from an array of VertexData obtained from 
    /// the provided VertexDataList.
    /// </summary>
    /// <param name="list"></param>
    public void Load(VertexDataList list)
    {
        verts = list.VertexArray();

        GL.GenBuffers(1, out vboHandle);
        GL.BindBuffer(BufferTarget.ArrayBuffer, vboHandle);
        GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(verts.Length * 
           BlittableValueType.StrideOf(verts)), verts, 
           BufferUsageHint.StaticDraw);

        GL.GenVertexArrays(1, out vaoHandle);
        GL.BindVertexArray(vaoHandle);

        GL.EnableClientState(ArrayCap.VertexArray);
        GL.EnableClientState(ArrayCap.ColorArray);

        GL.VertexPointer(3, VertexPointerType.Float, 
           BlittableValueType.StrideOf(verts), (IntPtr)0);
        GL.ColorPointer(3, ColorPointerType.Float, 
           BlittableValueType.StrideOf(verts), (IntPtr)12);

        GL.BindVertexArray(0);
    }

   /// <summary>
   /// This method uses the vaoHandle to draw the "figure."
   /// </summary>
   public void Show()
    {
        GL.BindVertexArray(vaoHandle);
        GL.DrawArrays(PrimitiveType.Triangles, 0, verts.Length);
        GL.BindVertexArray(0);
    }

    /// <summary>
    /// Getter: Returns name
    /// Setter: Sets name to a provided filename without the ".wrl" extension
    /// </summary>
    public string Name
    {
        get { return name; }
        set
        {
            name = new FileInfo(value).Name;
            name = name.Substring(0, name.IndexOf(".wrl"));
        }
    }
}
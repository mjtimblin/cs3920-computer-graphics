﻿namespace Prog1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         this.glControl1 = new OpenTK.GLControl();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.figureTypeComboBox = new System.Windows.Forms.ComboBox();
         this.figureColorComboBox = new System.Windows.Forms.ComboBox();
         this.lineWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         ((System.ComponentModel.ISupportInitialize)(this.lineWidthNumericUpDown)).BeginInit();
         this.menuStrip1.SuspendLayout();
         this.SuspendLayout();
         // 
         // glControl1
         // 
         this.glControl1.BackColor = System.Drawing.Color.Black;
         this.glControl1.Location = new System.Drawing.Point(12, 74);
         this.glControl1.Name = "glControl1";
         this.glControl1.Size = new System.Drawing.Size(600, 600);
         this.glControl1.TabIndex = 0;
         this.glControl1.VSync = false;
         this.glControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseDown);
         this.glControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseMove);
         this.glControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseUp);
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(9, 31);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(63, 13);
         this.label1.TabIndex = 1;
         this.label1.Text = "Figure Type";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(148, 31);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(63, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Figure Color";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(288, 31);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(58, 13);
         this.label3.TabIndex = 3;
         this.label3.Text = "Line Width";
         // 
         // figureTypeComboBox
         // 
         this.figureTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.figureTypeComboBox.FormattingEnabled = true;
         this.figureTypeComboBox.Items.AddRange(new object[] {
            "Line",
            "Rectangle"});
         this.figureTypeComboBox.Location = new System.Drawing.Point(12, 47);
         this.figureTypeComboBox.Name = "figureTypeComboBox";
         this.figureTypeComboBox.Size = new System.Drawing.Size(121, 21);
         this.figureTypeComboBox.TabIndex = 0;
         // 
         // figureColorComboBox
         // 
         this.figureColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.figureColorComboBox.FormattingEnabled = true;
         this.figureColorComboBox.Items.AddRange(new object[] {
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue",
            "Indigo",
            "Violet",
            "White"});
         this.figureColorComboBox.Location = new System.Drawing.Point(151, 47);
         this.figureColorComboBox.Name = "figureColorComboBox";
         this.figureColorComboBox.Size = new System.Drawing.Size(121, 21);
         this.figureColorComboBox.TabIndex = 5;
         // 
         // lineWidthNumericUpDown
         // 
         this.lineWidthNumericUpDown.Location = new System.Drawing.Point(291, 47);
         this.lineWidthNumericUpDown.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
         this.lineWidthNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
         this.lineWidthNumericUpDown.Name = "lineWidthNumericUpDown";
         this.lineWidthNumericUpDown.Size = new System.Drawing.Size(76, 20);
         this.lineWidthNumericUpDown.TabIndex = 6;
         this.lineWidthNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
         // 
         // menuStrip1
         // 
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Size = new System.Drawing.Size(624, 24);
         this.menuStrip1.TabIndex = 8;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // fileToolStripMenuItem
         // 
         this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
         this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
         this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
         this.fileToolStripMenuItem.Text = "&File";
         // 
         // saveToolStripMenuItem
         // 
         this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
         this.saveToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
         this.saveToolStripMenuItem.Text = "&Save";
         this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
         // 
         // exitToolStripMenuItem
         // 
         this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
         this.exitToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
         this.exitToolStripMenuItem.Text = "E&xit";
         this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
         // 
         // MainForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(624, 686);
         this.Controls.Add(this.lineWidthNumericUpDown);
         this.Controls.Add(this.figureColorComboBox);
         this.Controls.Add(this.figureTypeComboBox);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.glControl1);
         this.Controls.Add(this.menuStrip1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MainMenuStrip = this.menuStrip1;
         this.Name = "MainForm";
         this.Text = "Line / Rectangle Maker - Drag to make them!";
         this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
         this.Load += new System.EventHandler(this.MainForm_Load);
         this.Shown += new System.EventHandler(this.MainForm_Shown);
         this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
         ((System.ComponentModel.ISupportInitialize)(this.lineWidthNumericUpDown)).EndInit();
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox figureTypeComboBox;
        private System.Windows.Forms.ComboBox figureColorComboBox;
        private System.Windows.Forms.NumericUpDown lineWidthNumericUpDown;
      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
   }
}


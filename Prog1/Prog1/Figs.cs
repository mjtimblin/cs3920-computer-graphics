/**
 * Name:    Michael Timblin
 * Purpose: This file contains code for the Figure, TRectangle, and TLine
 *          classes. These classes contain methods for creating and
 *          drawing OpenGL primitives.
 * */

using System.Collections.Generic;
using System.Drawing;
using OpenTK.Graphics.OpenGL;

namespace Figures
{
   public abstract class Figure
   {
      protected List<Point> pts = new List<Point>();
      protected Color fgColor = Color.White;
      protected float lineWidth = 1.0F;

      public abstract void Show();

      public Color FGColor
      {
         get { return fgColor; }
         set { fgColor = value; }
      }

      public float LineWidth
      {
         get { return lineWidth; }
         set { lineWidth = value; }
      }

      public void AddPoint ( Point p ) { pts.Add(p); }

      public void ReplacePoint ( Point p, int index ) { pts[index] = p; }
   }

   public class TLine : Figure
   {
      /// <summary>
      ///  Show figure
      /// </summary>
      public override void Show() 
      {
         GL.Color3(fgColor.R, fgColor.G, fgColor.B);
         GL.LineWidth(lineWidth);
         GL.Begin(PrimitiveType.LineStrip);
            GL.Vertex2(pts[0].X, pts[0].Y);
            GL.Vertex2(pts[1].X, pts[1].Y);
         GL.End();
      }
   }

   public class TRectangle : Figure
   {
      public override void Show()
      {
         GL.Color3(fgColor);
         GL.LineWidth(lineWidth);

         GL.Begin(PrimitiveType.LineLoop);
            GL.Vertex2(pts[0].X, pts[0].Y);
            GL.Vertex2(pts[1].X, pts[0].Y);
            GL.Vertex2(pts[1].X, pts[1].Y);
            GL.Vertex2(pts[0].X, pts[1].Y);
         GL.End();
      }
   }
}
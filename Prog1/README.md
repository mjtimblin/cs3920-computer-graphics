## Prog1
---
### Details
- Program: 1
- Points: 12
- Due Date: September 26
- Grace Date: September 29 - Demonstrated no later than 5:00 p.m.

### To Do
- (Optional) Add extra functionality
  - Turtle Mode with turtle image and arrow keys to move forward, turn left, and turn right and click to put pen up and down. On keypressDown enable timer for moving forward or timer for turning.
  - Add saving and loading of drawings
  - Add export drawing as image

---
© 2017 Michael Timblin

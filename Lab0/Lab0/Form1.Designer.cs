﻿namespace Lab0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.glControl1 = new OpenTK.GLControl();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnChangeColor = new System.Windows.Forms.Button();
            this.txtVertexX = new System.Windows.Forms.TextBox();
            this.txtVertexY = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddVertex = new System.Windows.Forms.Button();
            this.btnDeleteVertices = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // glControl1
            // 
            this.glControl1.BackColor = System.Drawing.Color.Black;
            this.glControl1.Location = new System.Drawing.Point(12, 12);
            this.glControl1.Name = "glControl1";
            this.glControl1.Size = new System.Drawing.Size(500, 500);
            this.glControl1.TabIndex = 0;
            this.glControl1.VSync = false;
            this.glControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseClick);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(415, 519);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(99, 23);
            this.btnShow.TabIndex = 1;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(415, 548);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(99, 23);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnChangeColor
            // 
            this.btnChangeColor.Location = new System.Drawing.Point(12, 518);
            this.btnChangeColor.Name = "btnChangeColor";
            this.btnChangeColor.Size = new System.Drawing.Size(106, 23);
            this.btnChangeColor.TabIndex = 3;
            this.btnChangeColor.Text = "Change Color";
            this.btnChangeColor.UseVisualStyleBackColor = true;
            this.btnChangeColor.Click += new System.EventHandler(this.btnChangeColor_Click);
            // 
            // txtVertexX
            // 
            this.txtVertexX.Location = new System.Drawing.Point(83, 553);
            this.txtVertexX.Name = "txtVertexX";
            this.txtVertexX.Size = new System.Drawing.Size(100, 20);
            this.txtVertexX.TabIndex = 4;
            // 
            // txtVertexY
            // 
            this.txtVertexY.Location = new System.Drawing.Point(83, 579);
            this.txtVertexY.Name = "txtVertexY";
            this.txtVertexY.Size = new System.Drawing.Size(100, 20);
            this.txtVertexY.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 553);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "X (-1.0 - 1.0)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 579);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Y (-1.0 - 1.0)";
            // 
            // btnAddVertex
            // 
            this.btnAddVertex.Location = new System.Drawing.Point(200, 551);
            this.btnAddVertex.Name = "btnAddVertex";
            this.btnAddVertex.Size = new System.Drawing.Size(114, 23);
            this.btnAddVertex.TabIndex = 8;
            this.btnAddVertex.Text = "Add Vertex";
            this.btnAddVertex.UseVisualStyleBackColor = true;
            this.btnAddVertex.Click += new System.EventHandler(this.btnAddVertex_Click);
            // 
            // btnDeleteVertices
            // 
            this.btnDeleteVertices.Location = new System.Drawing.Point(200, 580);
            this.btnDeleteVertices.Name = "btnDeleteVertices";
            this.btnDeleteVertices.Size = new System.Drawing.Size(114, 23);
            this.btnDeleteVertices.TabIndex = 9;
            this.btnDeleteVertices.Text = "Remove All Vertices";
            this.btnDeleteVertices.UseVisualStyleBackColor = true;
            this.btnDeleteVertices.Click += new System.EventHandler(this.btnDeleteVertices_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 615);
            this.Controls.Add(this.btnDeleteVertices);
            this.Controls.Add(this.btnAddVertex);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtVertexY);
            this.Controls.Add(this.txtVertexX);
            this.Controls.Add(this.btnChangeColor);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.glControl1);
            this.Name = "Form1";
            this.Text = "My First OpenGL Lab";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnChangeColor;
        private System.Windows.Forms.TextBox txtVertexX;
        private System.Windows.Forms.TextBox txtVertexY;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddVertex;
        private System.Windows.Forms.Button btnDeleteVertices;
    }
}


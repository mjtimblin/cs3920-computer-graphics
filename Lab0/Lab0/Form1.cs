﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;

namespace Lab0
{
    public partial class Form1 : Form
    {
        private double primaryColorRed = 0.0;
        private double primaryColorGreen = 0.0;
        private double primaryColorBlue = 0.0;
        private Timer timer = new Timer();
        private double[] vertexXPsitions = new double[] { -0.8, -0.8, 0.8, 0.8 };
        private double[] vertexYPsitions = new double[] { -0.8, 0.8, 0.8, -0.8 };
        private List<double> vertexXPositions = new List<double>();
        private List<double> vertexYPositions = new List<double>();


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer.Enabled = false;
            timer.Interval = 100;
            timer.Tick += new System.EventHandler(timer_Tick);
            vertexXPositions.Add(-0.8);
            vertexXPositions.Add(-0.8);
            vertexXPositions.Add(0.8);
            vertexXPositions.Add(0.8);

            vertexYPositions.Add(-0.8);
            vertexYPositions.Add(0.8);
            vertexYPositions.Add(0.8);
            vertexYPositions.Add(-0.8);
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Begin(PrimitiveType.Polygon);
            GL.Color3(primaryColorRed, primaryColorGreen, primaryColorBlue);
            for (int i = 0; i < vertexXPositions.Count; i++)
                GL.Vertex2(vertexXPositions[i], vertexYPositions[i]);
            GL.End();
            glControl1.SwapBuffers();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            glControl1.SwapBuffers();
        }

        private void btnChangeColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
                primaryColorRed = (1.0f / 255) * colorDialog.Color.R;
                primaryColorGreen = (1.0f / 255) * colorDialog.Color.G;
                primaryColorBlue = (1.0f / 255) * colorDialog.Color.B;
        }

        private void glControl1_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void timer_Tick(object sender, EventArgs e)
        {

        }

        private void btnAddVertex_Click(object sender, EventArgs e)
        {
            double xPosition = Double.Parse(txtVertexX.Text);
            double yPosition = Double.Parse(txtVertexY.Text);

            if (xPosition >= -1 && xPosition <= 1 && yPosition >= -1 && yPosition <= 1)
                vertexXPositions.Add(xPosition);
                vertexYPositions.Add(yPosition);
        }

        private void btnDeleteVertices_Click(object sender, EventArgs e)
        {
            vertexXPositions.Clear();
            vertexYPositions.Clear();
        }
    }
}

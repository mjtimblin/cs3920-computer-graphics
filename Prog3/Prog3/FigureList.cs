﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file contains varaiables and methods necissary for holding
 *          and interacting with a list of figures loaded from a given
 *          directory.
 */

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.IO;

namespace Prog3
{
   /// <summary>
   /// figure list class is for holding a
   /// list of figures
   /// </summary>
   class FigureList
   {
      /// <summary>
      /// struct holds the glue that connects
      /// figure class and its connected movement pattern
      /// </summary>
      private struct FigureMovementPair
      {
         public Figure fig;
         public MovePattern movement;
      }

      private const int MAX_NUM_PATTERNS = 4;
      private const int PATTERN_ONE = 0;
      private const int PATTERN_TWO = 1;
      private const int PATTERN_THREE = 2;
      private const int PATTERN_FOUR = 3;

      private List<FigureMovementPair> figList =
          new List<FigureMovementPair>();

      /// <summary>
      /// Load figures from file and puts them 
      /// in a list, associates movement to them
      /// in sequence
      /// </summary>
      /// <param name="folderName"></param>
      public void LoadFigures(string folderName)
      {
         int patNum = PATTERN_ONE;
         string[] files = Directory.GetFiles(folderName);
         foreach (string filePath in files)
         {
            if (filePath.EndsWith(".wrl"))
            {
               VertexDataList list = new VertexDataList();
               if (list.LoadDataFromVRML(filePath))
               {
                  Figure fig = new Figure();
                  fig.Load(list);
                  fig.Name = filePath;
                  FigureMovementPair newPair =
                      new FigureMovementPair();
                  newPair.fig = fig;
                  switch (patNum)
                  {
                     case PATTERN_ONE:
                        newPair.movement =
                            new LateralusMovePattern();
                        break;
                     case PATTERN_TWO:
                        newPair.movement =
                            new StretchContractMovePattern();
                        break;
                     case PATTERN_THREE:
                        newPair.movement =
                            new TopMovePattern();
                        break;
                     case PATTERN_FOUR:
                        newPair.movement =
                            new ConeMovePattern(newPair.fig);
                        break;
                  }
                  patNum = (patNum + 1) % MAX_NUM_PATTERNS;
                  figList.Add(newPair);
               }
            }
         }
      }
      /// <summary>
      /// show the list of figures on the screen
      /// </summary>
      /// <param name="lookAt"></param>
      public void Show(Matrix4 lookAt)
      {
         foreach (FigureMovementPair pair in figList)
         {
            pair.fig.Show(lookAt);
         }
      }
      /// <summary>
      /// move the figures based on correlated move pattern. 
      /// </summary>
      public void Move()
      {
         foreach (FigureMovementPair pair in figList)
         {
            pair.movement.Move(pair.fig);
         }
      }
      //method for resetting figures
      public void Restore()
      {
         foreach (FigureMovementPair pair in figList)
         {
            pair.fig.Restore();
         }
      }
   }
}

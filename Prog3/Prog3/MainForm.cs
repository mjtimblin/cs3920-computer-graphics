﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This is the main form for displaying and interfacing with OPENGL
 *          to display 3D models
 */

using System;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Prog3
{
   /// <summary>
   /// Main form initializing components and classes
   /// </summary>
   public partial class MainForm : Form
   {
      private readonly float WINDOW_SIZE = 1.5f;
      private readonly float WIN_NEAR = 2.0f;
      private readonly float WIN_FAR = 80.0f;
      private readonly int MAX_TIMER_DURATION = 2000;
      private readonly int trackBarStartingValue = 50;
      private readonly float startingPosition = 5.0f;

      private FigureList figList = new FigureList();
      private Vector3 eye;

      /// <summary>
      /// Constructor: This initaializes graphical elements and initializes
      /// eye to its starting position.
      /// </summary>
      public MainForm()
      {
         InitializeComponent();
         eye = new Vector3(startingPosition, startingPosition,
            startingPosition);
      }

      /// <summary>
      /// This method is called when the LoadFigures menu item is selected.
      /// It opens a FolderBrowserDialog and calls figList.LoadFigures on the
      /// chosen folder.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void menuFileLoadFigures_Click(object sender, EventArgs e)
      {
         FolderBrowserDialog folderBrowserDialog1 =
                new FolderBrowserDialog();
         folderBrowserDialog1.SelectedPath =
            "K:\\Courses\\CSSE\\tianb\\cs3920_cs5920\\jon_tim\\Prog3\\figures";
         folderBrowserDialog1.ShowDialog();
         
         if (folderBrowserDialog1.SelectedPath != "")
         {
            figList.LoadFigures(folderBrowserDialog1.SelectedPath);
         }
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the Exit menu item is selected. It simply
      /// exits the program.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void menuFileExit_Click(object sender, EventArgs e)
      {
         Application.Exit();
      }

      /// <summary>
      /// calls open gl methods to allow orthographic view on load event
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_Load(object sender, EventArgs e)
      {
         GL.Enable(EnableCap.DepthTest);
         float mult = (float)glControl1.Height / (float)glControl1.Width;
         Matrix4 projMat = Matrix4.CreatePerspectiveOffCenter(
             -WINDOW_SIZE, WINDOW_SIZE, -WINDOW_SIZE * mult,
             WINDOW_SIZE * mult, WIN_NEAR, WIN_FAR);
         GL.MatrixMode(MatrixMode.Projection);
         GL.LoadMatrix(ref projMat);
         GL.MatrixMode(MatrixMode.Modelview);
         ShowFigures();
      }

      /// <summary>
      /// shows 3D figure
      /// </summary>
      private void ShowFigures()
      {
         GL.Clear(ClearBufferMask.ColorBufferBit |
             ClearBufferMask.DepthBufferBit);
         Matrix4 lookAt = Matrix4.LookAt(eye.X, eye.Y, eye.Z, 0.0f, 0.0f,
             0.0f, 0.0f, 1.0f, 0.0f);
         figList.Show(lookAt);
         Axes.Instance.Show(lookAt);
         GL.MatrixMode(MatrixMode.Modelview);
         glControl1.SwapBuffers();
      }

      /// <summary>
      /// shows axes for initial load of program
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_Shown(object sender, EventArgs e)
      {
         ShowFigures();
      }

      /// <summary>
      /// shows figure when size of window is changed
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void MainForm_SizeChanged(object sender, EventArgs e)
      {
         glControl1.SwapBuffers();
      }

      /// <summary>
      /// changes viewing handle of x-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarX_ValueChanged(object sender, EventArgs e)
      {
         float xPosition = trackBarX.Value / 10.0f;
         eye = new Vector3(xPosition, eye.Y, eye.Z);
         labelX.Text = "X = " + xPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// changes viewing handle of y-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarY_ValueChanged(object sender, EventArgs e)
      {
         float yPosition = trackBarY.Value / 10.0f;
         eye = new Vector3(eye.X, yPosition, eye.Z);
         labelY.Text = "Y = " + yPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// changes viewing handle of z-axis based on slide value
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarZ_ValueChanged(object sender, EventArgs e)
      {
         float zPosition = trackBarZ.Value / 10.0f;
         eye = new Vector3(eye.X, eye.Y, zPosition);
         labelZ.Text = "Z = " + zPosition.ToString("0.0");
         ShowFigures();
      }

      /// <summary>
      /// This method is called when the btnReset button is clicked. The
      /// method resets the eye to the starting position.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnReset_Click(object sender, EventArgs e)
      {
         eye = new Vector3(startingPosition, startingPosition,
            startingPosition);
         trackBarX.Value = trackBarStartingValue;
         trackBarY.Value = trackBarStartingValue;
         trackBarZ.Value = trackBarStartingValue;
         labelX.Text = labelY.Text = labelZ.Text = "X = " +
                startingPosition.ToString("0.0");
         figList.Restore();
         ShowFigures();
      }
      /// <summary>
      /// scales time between movement based on a slider
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void trackBarTimer_ValueChanged(object sender, EventArgs e)
      {
         if (trackBarTimer.Value == 0)
            movementTimer.Enabled = false;
         else
         {
            movementTimer.Interval = MAX_TIMER_DURATION /
                trackBarTimer.Value;
            movementTimer.Enabled = true;
         }
         labelTimer.Text = "Movement speed = " +
             trackBarTimer.Value + "%";
      }

      private void movementTimer_Tick(object sender, EventArgs e)
      {
         figList.Move();
         ShowFigures();
      }
   }
}
﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file is holds the movepattern abstract class and four child
 *          classes that translate, rotate, and scale various given figures.
 */

using System;
using OpenTK;

namespace Prog3
{
   // This is an abstract class for moving figures with a move method
   abstract class MovePattern
   {
      /// <summary>
      /// This method is an abstract method to perform transformations on a
      /// given figure.
      /// </summary>
      /// <param name="fig"></param>
      abstract public void Move(Figure fig);
   }

   // This class revolves and rotates a given figure around the y axis.
   class LateralusMovePattern : MovePattern
   {
      private readonly double RADIUS = 1;
      private readonly double INTERVAL = 0.1;

      private float fibA;
      private float fibB;
      private const int scalefx = 100;
      private bool forward = true;
      private double theta = 5;
      private double alpha = 0;
      private const float ANGLE = 10;
      private float scale;
      private Vector3 cp;

      /// <summary>
      /// Constructor: This initializes figA and fibB to 1.
      /// </summary>
      public LateralusMovePattern()
      {
         fibA = fibB = 1;
      }

      /// <summary>
      /// This method rotates and translates a given figure in order to make
      /// it revolve around the y axis.
      /// </summary>
      /// <param name="fig"></param>
      public override void Move(Figure fig)
      {
         float temp = fibB;
         fibB = fibA + fibB;
         fibA = temp;
         if (forward == true)
            scale = (scalefx + fibA) / scalefx;
         else
            scale = scalefx / (scalefx + fibA);
         if (fibA > 21)
         {
            fibA = fibB = 1;
            if (forward == true)
               forward = false;
            else
               forward = true;
         }
         cp = fig.CurrentCenter;
         double xAng = RADIUS * Math.Cos(theta);
         float yAng = 0;
         double zAng = RADIUS * Math.Sin(theta);
         double dDeltaX = zAng * Math.Cos(alpha) - xAng * Math.Sin(alpha);
         double dDeltaZ = xAng * Math.Cos(alpha) + zAng * Math.Sin(alpha);
         float deltaX = (float)dDeltaX;
         float deltaZ = (float)dDeltaZ;
         fig.Translate(deltaX, yAng, deltaZ);
         fig.Rotate(new Vector3(scale, scale, scale), ANGLE);
         alpha += INTERVAL;
      }
   }

   // This class stretches and contracts a given figure in relation to the
   // x, y, and z axes.
   class StretchContractMovePattern : MovePattern
   {
      private readonly float INITIAL_X_SCALE = 1.05f;
      private readonly float INITIAL_Y_Z_SCALES = 1.0f;
      private readonly float SCALE_INC = 1.05f;
      private readonly float SCALE_DEC = 0.95f;
      private readonly int MAX_COUNT = 25;
      private readonly int MIN_COUNT = 2;

      private int count;
      private bool reverse;
      private float scaleX;
      private float scaleY;
      private float scaleZ;

      /// <summary>
      /// Constructor: This initializes class variables.
      /// </summary>
      public StretchContractMovePattern()
      {
         count = 1;
         reverse = false;
         scaleX = INITIAL_X_SCALE;
         scaleY = scaleZ = INITIAL_Y_Z_SCALES;
      }

      /// <summary>
      /// This method performs scalings on a given figure to stretch and
      /// contract the figure in relation to the x, y, and z axes.
      /// </summary>
      /// <param name="fig"></param>
      public override void Move(Figure fig)
      {
         if (reverse == false)
         {
            fig.Scale(scaleX, scaleY, scaleZ);
            count++;
         }
         else
         {
            fig.Scale(scaleX, scaleY, scaleZ);
            count--;
         }
         if (count > MAX_COUNT)
         {
            reverse = true;
            if (scaleX == SCALE_INC)
               scaleX = SCALE_DEC;
            if (scaleY == SCALE_INC)
               scaleY = SCALE_DEC;
            if (scaleZ == SCALE_INC)
               scaleZ = SCALE_DEC;
         }
         if (count < MIN_COUNT)
         {
            reverse = false;
            if (scaleX == SCALE_DEC)
            {
               scaleX = 1.0f;
               scaleY = SCALE_INC;
            }
            if (scaleY == SCALE_DEC)
            {
               scaleY = 1.0f;
               scaleZ = SCALE_INC;
            }
            if (scaleZ == SCALE_DEC)
            {
               scaleZ = 1.0f;
               scaleX = SCALE_INC;
            }
         }
      }
   }

   // This class moves the given figure in top like manner. The figure will
   // also flip over the XZ plane randomly to simulate gravity changing.
   class TopMovePattern : MovePattern
   {
      private readonly float SCALE_PERCENT = 0.05f;
      private readonly float ROTATION_PER_ITERATION = 5.0f;
      private readonly int MAX_RANDOM_NUMBER = 10;
      private readonly int FLIP_OVER_PLANE_NUMBER = 0;
      private readonly int SCALE_DOWN_NUMBER = 1;
      private readonly int SCALE_UP_NUMBER = 2;

      private Random random = new Random();

      /// <summary>
      /// This method performs translations, rotations, and scalings on a
      /// given figure to rotate the figure around itself like a top. The
      /// figure randomly scales smaller and larger by SCALE_PERCENT of 
      /// its current size. Also, the figure will flip over the XZ plane 
      /// randomly to simulate gravity changing.
      /// </summary>
      /// <param name="fig"></param>
      public override void Move(Figure fig)
      {
         Vector3 position = fig.CurrentCenter;
         int randomNum = random.Next(MAX_RANDOM_NUMBER);

         if (randomNum == FLIP_OVER_PLANE_NUMBER)
         {
            fig.Translate(0, -1 * position.Y, 0);
            fig.Rotate(new Vector3(1.0f, 0.0f, 0.0f), 180.0f);
         }
         else if (randomNum == SCALE_DOWN_NUMBER)
         {
            fig.Scale(1.0f - SCALE_PERCENT, 1.0f - SCALE_PERCENT,
               1.0f - SCALE_PERCENT);
         }
         else if (randomNum == SCALE_UP_NUMBER)
         {
            fig.Scale(1.0f + SCALE_PERCENT, 1.0f + SCALE_PERCENT,
               1.0f + SCALE_PERCENT);
         }

         fig.Rotate(new Vector3(0.0f, 1.0f, 0.0f), ROTATION_PER_ITERATION);
      }
   }

   // This class moves a given figure in a funnel shape moving upwards.
   class ConeMovePattern : MovePattern
   {
      private readonly float HEIGHT_CHANGE_PER_ITERATION = 0.01f;
      private readonly float ROTATION_PER_ITERATION = 1.0f;
      private readonly float RADIUS_FACTOR = 0.1f;

      private int iteration = 1;
      private Vector3 startPosition;

      /// <summary>
      /// Constructor: This takes a figure as a parameter and sets 
      /// start position equal to the given figure's CurrentCenter.
      /// </summary>
      /// <param name="fig"></param>
      public ConeMovePattern(Figure fig)
      {
         startPosition = fig.CurrentCenter;
      }

      /// <summary>
      /// This method performs translations and rotations on the given figure
      /// to make the figure move in a funnel shape moving upwards around
      /// the center of the figure at its starting position.
      /// </summary>
      /// <param name="fig"></param>
      public override void Move(Figure fig)
      {
         Vector3 position = fig.CurrentCenter;

         if (position == startPosition)
            iteration = 1;

         float xPos = (float)Math.Cos(iteration);
         float zPos = (float)Math.Sin(iteration);

         float radius = iteration * RADIUS_FACTOR;

         fig.Translate(-1 * position.X, -1 * position.Y, -1 * position.Z);
         fig.Translate((xPos - fig.CurrentCenter.X) * radius,
            HEIGHT_CHANGE_PER_ITERATION, (zPos - fig.CurrentCenter.Z) *
            radius);
         fig.Translate(position.X, position.Y, position.Z);
         fig.Rotate(new Vector3(0.0f, 1.0f, 0.0f), ROTATION_PER_ITERATION);

         iteration++;
      }
   }
}
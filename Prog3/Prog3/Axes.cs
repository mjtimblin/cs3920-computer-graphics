﻿/*
 * Name:    Michael Timblin and Tod Jones
 * Purpose: This file contains code to create and show X, Y, and Z axes.
 */

using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

/// <summary>
/// This class creates and draws axes.
/// </summary>
public class Axes
{
   private static Axes _instance = null;
   private int vboHandle;
   private int vaoHandle;

   private VertexData[] verts =
   {
      new VertexData(new Vector3(0.0f, 0.0f, 0.0f),
         new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)),
      new VertexData(new Vector3(200.0f, 0.0f, 0.0f),
         new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)),
      new VertexData(new Vector3(0.0f, 0.0f, 0.0f),
         new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 1.0f)),
      new VertexData(new Vector3(0.0f, 200.0f, 0.0f),
         new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)),
      new VertexData(new Vector3(0.0f, 0.0f, 0.0f),
         new Vector3(0.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)),
      new VertexData(new Vector3(0.0f, 0.0f, 200.0f),
         new Vector3(0.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f))
   };

   /// <summary>
   /// This method creates and returns an instance of Axes.
   /// </summary>
   public static Axes Instance
   {
      get
      {
         if (_instance == null)
            _instance = new Axes();
         return _instance;
      }
   }

   /// <summary>
   /// Constructor: This creates the vertex buffer object (vboHandle) and 
   /// vertex array object (vaoHandle).
   /// </summary>
   private Axes()
   {
      GL.GenBuffers(1, out vboHandle);
      GL.BindBuffer(BufferTarget.ArrayBuffer, vboHandle);
      GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(verts.Length *
         BlittableValueType.StrideOf(verts)), verts,
         BufferUsageHint.StaticDraw);

      GL.GenVertexArrays(1, out vaoHandle);
      GL.BindVertexArray(vaoHandle);

      GL.EnableClientState(ArrayCap.VertexArray);
      GL.EnableClientState(ArrayCap.ColorArray);

      GL.VertexPointer(3, VertexPointerType.Float,
         BlittableValueType.StrideOf(verts), (IntPtr)0);
      GL.ColorPointer(3, ColorPointerType.Float,
         BlittableValueType.StrideOf(verts), (IntPtr)12);

      GL.BindVertexArray(0);
   }

   /// <summary>
   /// This method uses the vaoHandle to draw the axes.
   /// </summary>
   public void Show(Matrix4 lookAt)
   {
      GL.BindVertexArray(vaoHandle);
      GL.MatrixMode(MatrixMode.Modelview);
      GL.LoadMatrix(ref lookAt);
      GL.DrawArrays(PrimitiveType.Lines, 0, verts.Length);
      GL.BindVertexArray(0);
   }
}
